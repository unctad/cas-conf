version: "3.7"

services:

  portainer:
    image: portainer/portainer:1.24.0 # latest stable version
    ports:
      - "9000:9000"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /opt/portainer/data:/data

  graylog:
    image: unctad/graylog:LIVE5 # latest LIVE version
    environment:
      - "GRAYLOG_WEB_ENDPOINT_URI=https://graylog.YOUR_DOMAIN_NAME/api" #changeme
      - "GRAYLOG_REST_TRANSPORT_URI=https://graylog.YOUR_DOMAIN_NAME/api/" #changeme
      - "GRAYLOG_ROOT_PASSWORD_SHA2=DOCKER_SECRET:GRAYLOG_ROOT_PASSWORD"
      - "GRAYLOG_MONGODB_URI=DOCKER_SECRET:GRAYLOG_MONGODB_URI"
      - "GRAYLOG_TRANSPORT_EMAIL_WEB_INTERFACE_URL=https://graylog.YOUR_DOMAIN_NAME" #changeme
      - "GRAYLOG_TRANSPORT_EMAIL_HOSTNAME=email-hostname.com" #changeme
      - "GRAYLOG_TRANSPORT_EMAIL_PORT=587" #changeme
      - "GRAYLOG_TRANSPORT_EMAIL_AUTH_USERNAME=username" #changeme
      - "GRAYLOG_TRANSPORT_EMAIL_AUTH_PASSWORD=DOCKER_SECRET:GRAYLOG_EMAIL_SERVER_PASSWORD"
      - "GRAYLOG_TRANSPORT_EMAIL_USE_AUTH=true"
      - "GRAYLOG_TRANSPORT_EMAIL_USE_TLS=false"
      - "GRAYLOG_TRANSPORT_EMAIL_USE_SSL=false"
      - "GRAYLOG_TRANSPORT_EMAIL_FROM_EMAIL=noreply@eregistrations.org" #changeme
    extra_hosts:
      - "mongodb_host:172.18.0.1"
      - "elastic_host:172.18.0.1"
    ports:
      - "9005:9005"
      - "12201:12201"
    secrets:
      - GRAYLOG_EMAIL_SERVER_PASSWORD
      - GRAYLOG_ROOT_PASSWORD
      - GRAYLOG_MONGODB_URI
    networks:
      cas_default:
      bridge:

  backend:
    ports:
      - "8282:8282"
    extra_hosts:
      - "postgres_host:172.18.0.1"
      - "stat_host:172.18.0.1"
    environment:
      - "SYSTEM_CODE=CH" #changeme
      - "DEFAULT_LANGUAGE=en" #changeme
      - "HTTPS_REQUIRED=true"
      - "OAUTH_CLIENT_HTTPS_REQUIRED=true"

      - "SPRING_DATASOURCE_URL=jdbc:postgresql://postgres_host:5432/cas"
      - "SPRING_DATASOURCE_USERNAME=cas"
      - "SPRING_DATASOURCE_PASSWORD=DOCKER_SECRET:CAS_DB_PASSWORD"

      - "STATISTICS_URI=http://stat_host:9200/cas/"
      - "STATISTICS_USERNAME=elastic"
      - "STATISTICS_PASSWORD=DOCKER_SECRET:CAS_STATISTICS_PASSWORD"

      - "MAIL_HOST=email-hostname.com" #changeme
      - "MAIL_PORT=587" #changeme
      - "MAIL_USERNAME=username" #changeme
      - "MAIL_PASSWORD=DOCKER_SECRET:CAS_EMAIL_SERVER_PASSWORD"
      - "MAIL_REPLY_TO=noreply@eregistrations.org" #changeme
      - "MAIL_FROM=noreply@eregistrations.org" #changeme
      - "EMAIL_PROD=true"
      - "EMAIL_VERIFICATION_EXPIRE_HOURS=1"
      - "ENV_MAIL_TRANSPORT_PROTOCOL=smtp"
      - "ENV_MAIL_SMTP_AUTH=true"
      - "ENV_MAIL_SMTPS_SSL_CHECK_SERVER_IDENTITY=false"
      - "ENV_MAIL_SMTPS_SSL_TRUST=*"
      - "ENV_MAIL_SMTP_SSL_ENABLE=false"
      - "ENV_MAIL_SMTP_STARTTLS_ENABLE=false"
      - "ENV_MAIL_DEBUG=true"

      - "TOKEN_DURATION=3"
      - "TOKEN_BLACKLIST=false"
      - "TOKEN_STRICT_LOGOUT=false"
      - "TOKEN_STRICT_PING_INTERVAL=5000"
      - "TOKEN_STRICT_RECONNECT_INTERVAL=5000"

      - "ACTIVEMQ_ENABLED=false"
      - "ACTIVEMQ_PASSWORD=DOCKER_SECRET:ACTIVEMQ_PASSWORD"

      - "MAX_FAILED_LOGIN_ATTEMPTS=30"
      - "ALERT_ON_FAILED_LOGIN_ATTEMPTS=30"
      - "NEW_PASSWORD_ASKED=30"
      - "PASSWORD_RENEW_DAYS=365"
      - "PASSWORD_RESET_EXPIRE_HOURS=1"
      - "PASSWORD_QUALITY_CAPITALS=1"
      - "PASSWORD_QUALITY_LOWER=1"
      - "PASSWORD_QUALITY_DIGITS=1"
      - "PASSWORD_QUALITY_SPECIAL=1"
      - "PASSWORD_QUALITY_LEN_MIN=1"
      - "PASSWORD_QUALITY_LEN_MAX=1"
      - "PASSWORD_QUALITY_ALLOW_WHITESPACE=true"
      - "PASSWORD_QUALITY_PHRASE_LEN_MIN=6"

      - "GOOGLE_RECAPTCHA_ENABLED=false"
      - "GOOGLE_RECAPTCHA_SITE_SECRET=DOCKER_SECRET:GOOGLE_RECAPTCHA_SECRET"
      - "GOOGLE_RECAPTCHA_SITE_KEY=key" #changeme
      - "GOOGLE_RECAPTCHA_VERIFY_URL=https://www.google.com/recaptcha/api/siteverify"
      - "GOOGLE_RECAPTCHA_URL=https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit"
    image: unctad/casbackend:LIVE26 # latest live version
    secrets:
      - CAS_DB_PASSWORD
      - CAS_STATISTICS_PASSWORD
      - CAS_EMAIL_SERVER_PASSWORD
      - ACTIVEMQ_PASSWORD
      - GOOGLE_RECAPTCHA_SECRET
    networks:
      cas_default:
      bridge:

  frontend:
    ports:
      - "4401:80"
    image: unctad/casfrontend:LIVE14 # latest live version
    environment:
      - "SYSTEM_CODE=CH" #changeme
      - "AUTH_URL=https://eid.YOUR_DOMAIN_NAME/cback/v1.0/" #changeme
      - "OAUTH_CLIENT_ID=cas-client"
      - "OAUTH_SECRET=cas-client"
      - "CAS_BASE_URL=https://eid.YOUR_DOMAIN_NAME/cback/v1.0/cas/" #changeme
      - "DEFAULT_LANGUAGE=en" #changeme
      - "TOKEN_STRICT_LOGOUT="

secrets:
  CAS_DB_PASSWORD:
    external: true
  CAS_STATISTICS_PASSWORD:
    external: true
  CAS_EMAIL_SERVER_PASSWORD:
    external: true
  ACTIVEMQ_PASSWORD:
    external: true
  GOOGLE_RECAPTCHA_SECRET:
    external: true
  GRAYLOG_EMAIL_SERVER_PASSWORD:
    external: true
  GRAYLOG_ROOT_PASSWORD:
    external: true
  GRAYLOG_MONGODB_URI:
    external: true

networks:
  cas_default:
    driver: overlay
  bridge:
    external: true
    driver: bridge
